using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Botones : MonoBehaviour {


	public void NewGame(string Scene)
	{
		SceneManager.LoadScene (Scene);
	}

	public void ExitGame()
	{
		Application.Quit ();
	}
}
