﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoor : MonoBehaviour {
	public Animator Animator;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider other){
		Animator.SetBool ("Open", false);
		GameObject.FindObjectOfType<Recojer> ().enabled = false;

	}
}
