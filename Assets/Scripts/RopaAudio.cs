﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopaAudio : MonoBehaviour {

	private AudioSource audioSource;
	public AudioClip[] audio;
	//public AudioClip audioToalla

	// Use this for initialization
	void Start () {

		audioSource = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {

	}

	public void OlerFeo()
	{

		audioSource.PlayOneShot (audio [0]);
	}

}