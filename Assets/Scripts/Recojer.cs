﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;


public class Recojer : MonoBehaviour
{
	public Animator Animator;
	bool _puertaSalida = true;
	private int _contadorPastillas = 0;


	const string TOALLA_TAG = "Toalla";
	const string BAÑO_TAG = "Baño";
	const string ROPA_TAG = "ropa";
	const string SALIDA_TAG = "salida";
	const string RADIO_TAG = "Radio";
	const string PASTILLAS_TAG = "Pastillas";

	float _tiempoDeTecla = 5.0f;
	float _tiempoDeVida = 30.0f;
		
	public float sightLength;
	public bool debugSight;

	public bool mirando;
	public bool _onScreen;
	public bool _tomasteToalla;
	public bool _tomasteBaño;
	public bool _tomasteRopa;
	public bool _salidaLista;
	public bool _eventoBaño;
	public bool _pastillas;
	public bool _eventoDeDaño;
	private bool _noSpamTecla;
	private bool _bañoUnaVez;





	public Texture _teclaE;
	public Texture _teclaR;
	public RawImage _dotRojo;
	public Image _FadeBaño;
	public Image _pastilleroEnPantalla;
	public RawImage _danioPantalla;


	// Use this for initialization
	void Start () {
		
		_tomasteToalla = false;
		_tomasteRopa = false;
		_tomasteBaño = false;
		_salidaLista = false;
		_noSpamTecla = true;
		_bañoUnaVez = true;
		_pastillas = false;
		_eventoDeDaño = false;
		_onScreen = true;


		_FadeBaño.CrossFadeAlpha (0.0f, 0.1f, false);
		_danioPantalla.CrossFadeAlpha(0.0f, 0.0f, false);



	}
	
	// Update is called once per frame
	void Update ()
	{

		CastSight();
		PararDaño();
		//_tiempoDeTecla -= Time.deltaTime;

	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(Camera.main.transform.position, Camera.main.transform.position + (Camera.main.transform.forward * sightLength)); // alcance para la colision del raycast
	}

	void CastSight()
	{


		Ray ray = new Ray(Camera.main.transform.position,  (Camera.main.transform.forward * sightLength));
		if(debugSight) Debug.DrawRay(ray.origin, ray.direction * sightLength, Color.blue);

		RaycastHit hits;
		mirando = false;

		if(Physics.Raycast(ray, out hits))
		{
			//Sentencia para tomar la toalla
			if(hits.transform.gameObject.tag == TOALLA_TAG && hits.distance <= 2.0f)
			{
				mirando = true;
				if(Input.GetKeyUp("e"))
				{
					_tomasteToalla = true;

					Destroy(hits.transform.gameObject);
				}
			}

			//Sentencia para tomar un baño
			if (hits.transform.gameObject.tag == BAÑO_TAG && hits.distance <= 2.0f)
			{
				mirando = true;


				if (_tomasteToalla && Input.GetKeyUp ("e") && _bañoUnaVez) 
					{
						_tomasteBaño = true;

						//Reporodicr Audio del Baño, se carga desde el script de la regadera
						var Regadera = hits.collider.gameObject.GetComponent<RegaderaAudio> ();
						if (Regadera != null) 
						{
							Regadera.tomarUnBaño ();

						}
						
					_bañoUnaVez = false;


						
						//AQUI VA LA PANTALLA NEGRA

				
					_FadeBaño.CrossFadeAlpha(1.0f,1.0f,false);
					GetComponent<FirstPersonController> ().enabled = false;
					StartCoroutine (VolverAlHotel ());
					_bañoUnaVez = false;

					}

				//Si no se ha tomado la toalla y se intenta bañar

				else if(!_tomasteToalla && Input.GetKeyUp ("e") && _noSpamTecla)
				{
					var ToallaBuscar = hits.collider.gameObject.GetComponent<RegaderaAudio> ();
					if (ToallaBuscar != null) 
					{
						// el objeto en la vista es un regadera
						ToallaBuscar.DondeEstaToalla ();
					}

					_tiempoDeTecla= 20.0f;
					NoSpamSonidos ();
				}
			} 



			if(hits.transform.gameObject.tag == ROPA_TAG && hits.distance <= 2.0f)
			{
				mirando = true;
				if (_tomasteBaño) {
					
					if (Input.GetKeyUp ("e")) {
						_tomasteRopa = true;
						Destroy (hits.transform.gameObject);
					}
				} else if (!_tomasteRopa && Input.GetKeyUp ("e")){

					var AudioOlerFeo = hits.collider.gameObject.GetComponent<RopaAudio> ();
					if (AudioOlerFeo != null) 
					{
						// el objeto en la vista es un regadera
						AudioOlerFeo.OlerFeo ();
					}
				}
			}

			if(hits.transform.gameObject.tag == SALIDA_TAG && hits.distance <= 2.0f && _puertaSalida)
			{
				mirando = true;
				if (_tomasteRopa ) {
					if (Input.GetKeyUp ("e")) {
						_salidaLista = true;


						var PuertaSalida = hits.collider.gameObject.GetComponent<PuertaSalidaAudio> ();
						if (PuertaSalida != null) 
						{
							// el objeto en la vista es un regadera
							PuertaSalida.Puerta ();
						}
						Animator.SetBool ("Open", true);
						//Destroy (hits.transform.gameObject);
					}
				} else if (!_tomasteRopa && Input.GetKeyUp ("e")){

					var SinRopaNo = hits.collider.gameObject.GetComponent<PuertaSalidaAudio> ();
					if (SinRopaNo != null) 
					{
						// el objeto en la vista es un regadera
						SinRopaNo.NoSalir ();
					}
				}
			}




			//Prender el Radio

			if (hits.transform.gameObject.tag == RADIO_TAG && hits.distance <= 2.0f) 
			{
				mirando = true;
				if (Input.GetKeyUp ("e")) 
				{
					var RadioSuena = hits.collider.gameObject.GetComponent<RadioAudio> ();
					var Molestia = gameObject.GetComponent<AudiosPersonaje> ();
					if (RadioSuena != null) 
					{
						_eventoDeDaño = true;
						RadioSuena.Radio ();
						Molestia.Dolor ();

					}
				}
			}

			//Tomar Pastillas 

			if (hits.transform.gameObject.tag == PASTILLAS_TAG && hits.distance <= 2.0f) 
			{
				mirando = true;
				if (Input.GetKeyUp ("e")) 
				{
					_pastillas = true;
					_contadorPastillas ++;
					Destroy (hits.transform.gameObject);

				}
			}




		}		
	}

	void OnGUI()
	{
		if(_onScreen){

		GUI.Label (new Rect (Screen.width / 2- 310, Screen.height / 2 +165, 200, 100), "X " + _contadorPastillas);

	
		if (_eventoDeDaño) 
		{
				GUI.Label (new Rect (Screen.width / 2 + 40, Screen.height / 2 +60, 200, 100), "Presiona ");
				GUI.Label (new Rect (Screen.width / 2 + 120, Screen.height / 2 +60, 200, 100), "   para ingerir las pildoras");
				GUI.DrawTexture (new Rect (Screen.width / 2 + 100, Screen.height / 2+ 20 , 25, 100), _teclaR, ScaleMode.ScaleToFit, true, 1.0F);

		}

		if (mirando) {
			GUI.DrawTexture (new Rect (Screen.width / 2 + 20, Screen.height / 2 - 50, 25, 100), _teclaE, ScaleMode.ScaleToFit, true, 1.0F);

		}	

		_dotRojo.gameObject.SetActive (mirando);
		}
	}

	void NoSpamSonidos()
	{
		_tiempoDeTecla -= Time.deltaTime;

		if (_tiempoDeTecla > 0)
		{
			_noSpamTecla = false;
		}

		if(_tiempoDeTecla <= 0)
		{
			_noSpamTecla = true;

		}
	
	}

	IEnumerator VolverAlHotel()
	{	
		
		_pastilleroEnPantalla.gameObject.SetActive (false);
		_onScreen = false;

		yield return new WaitForSeconds (7);
		_FadeBaño.CrossFadeAlpha (0.0f, 15.0f, false);
		GetComponent<FirstPersonController> ().enabled = true;
		_pastilleroEnPantalla.gameObject.SetActive (true);
		_onScreen = true;

	}
		

	void PararDaño()
	{
		if(_eventoDeDaño)
		{
			if (Input.GetKeyUp ("r") && _pastillas) {
				
				_contadorPastillas--;
				_eventoDeDaño = false;
				_tiempoDeVida = 30;
				_pastillas = false;
				_danioPantalla.CrossFadeAlpha(0.0f, 1.0f, false);

			} else 
{
				_tiempoDeVida -= Time.deltaTime;
				_danioPantalla.CrossFadeAlpha(3.0f, _tiempoDeVida, false);


				print ("Mi vida idiota: "+_tiempoDeVida);
			}
		}
	}


}
