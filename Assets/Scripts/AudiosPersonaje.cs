﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudiosPersonaje : MonoBehaviour {


	private AudioSource audioSource;
	public AudioClip[] audio;

	// Use this for initialization
	void Start () {

		audioSource = GetComponent<AudioSource> ();

	}


	public void Dolor()
	{

		audioSource.PlayOneShot (audio [0]);
	}
}
